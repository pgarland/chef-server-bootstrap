#!/bin/bash
RUBY_VERSION='2.2.3'

sudo apt-get -y update
sudo apt-get -y install \
  git \
  build-essential \
  libssl-dev \
  zlib1g-dev \
  libreadline6-dev \
  libyaml-dev

if [ -d ~/.rbenv ]; then
    echo "rbenv already installed, remove ~/.rbenv if you want to bootstrap."
    exit 1
fi

git clone https://github.com/sstephenson/rbenv.git ~/.rbenv
git clone https://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build

if [ -f ~/.bash_profile ] || [ -h ~/.bash_profile ]; then
    echo "~/.bash_profile found, backing up to ~/.bash_profile.old"
    mv ~/.bash_profile{,.old}
fi

if [ -f ~/.bash_login ] || [ -h ~/.bash_login ]; then
    echo "~/.bash_login found, backing up to ~/.bash_login.old"
    mv ~/.bash_login{,.old}
fi

cat > ~/.gemrc <<-'EOF'
gem: --no-ri --no-rdoc
install: --no-ri --no-rdoc
update: --no-ri --no-rdoc

EOF

cat > ~/.bash_profile <<-'EOF'
export RBENV_ROOT=$HOME/.rbenv
if [ -d $RBENV_ROOT ]; then
  export PATH=$RBENV_ROOT/bin:$PATH
  eval "$(rbenv init -)"
fi

EOF
source ~/.bash_profile
rbenv install $RUBY_VERSION -v
rbenv global $RUBY_VERSION

gem install \
  rbenv-rehash \
  bundler \
  chef \
  ruby-shadow

ret=$?
if [ $ret -ne 0 ]; then
    echo "Unfortunately something went wrong..." >&2
else
    echo "Ready to cook!"
fi

exec $SHELL -l
exit 0